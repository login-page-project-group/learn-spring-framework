package com.in28minutes.learnspringframework;

public class BasicJavaGamingApp {

    public static void main(String[] args){

        var game = new MarioGame();
        //var game = new Pacman();
        var gameRunner = new GameRunner(game); // To run the games mario, pacman etc.
        gameRunner.run();
    }
}
