package com.in28minutes.learnspringframework.helloworld;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BasicJavaGamingSpringApp {

    public static void main(String[] args) {

        // Launch a spring context
        var context = new AnnotationConfigApplicationContext(HelloWorldConfig.class);

        // Configure the things we want Spring to manage - @Configuration
        System.out.println(context.getBean("name"));

        System.out.println(context.getBean("person"));
        System.out.println(context.getBean("address02"));
        System.out.println(context.getBean(Address.class));

        System.out.println(context.getBean("person02MethodCall"));
        System.out.println(context.getBean("person03ParameterCall"));

//        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);

    }

}
