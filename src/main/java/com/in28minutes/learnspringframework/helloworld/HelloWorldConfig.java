package com.in28minutes.learnspringframework.helloworld;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

record Person(String name, int age, Address address){};

record Address(String firstLine, String city){};

@Configuration
public class HelloWorldConfig {

    @Bean
    public String name(){
        return "Äbhishek";
    }

    @Bean
    public int age(){
        return 23;
    }

    @Bean
    public Person person(){
        return new Person("Lionel", 35, address());
    }

    @Bean(name = "address02")
    @Primary
    public Address address(){
        return new Address("Street 10","Rosario");
    }

    @Bean(name = "address03")
    @Qualifier("address3Qualifier")
    public Address address03(){
        return new Address("Maktampur","Kalaburgi");
    }

    @Bean // method Call bean
    public Person person02MethodCall(){
        return new Person(name(), age(), new Address("Street 1", "Airoli Navi Mumbai"));
    }

    @Bean // Parameter Call bean
    public Person person03ParameterCall(String name, int age, @Qualifier("address3Qualifier") Address address03){
        return new Person(name, age, address03);
    }

}
